FROM node:10

WORKDIR /usr/src/backtoschool
COPY . .
RUN yarn config set ignore-engines true
RUN yarn 


EXPOSE 8081
CMD [ "yarn", "start" ]
